<!-- 
Recommended titles:  
* New misused terms: term1, term2
* Updating misused term: term1

Use this template to propose an addition/update to the existing
list of misused terms at GitLab.

If you need help updating the YML, feel free to open an issue with
your proposal and someone will help you prepare an MR.
-->

## Misused term (or terms)

<!-- 
Words to avoid using... 
Feel free to list more than one, if it's applicable
-->

* `<word>`
* `<word>`
* ...

## Alternatives

<!-- 
Use one of these instead
-->

* `<word>`
* `<word>`
* `<word>`
* `<word>`

# Reason why

<!-- 
Please elaborate (while keeping it short and succinct) about the reasons why the alternatives are better.
-->

* ...


# Author checklist

* [ ] Named the MR appropriately
* [ ] Updated `data/misused_terms.yml` properly
* [ ] Reviewed how it renders on the Top Misused Terms page
* [ ] Submit for review

# Review process

<!-- 
Suggestion: To find an initial reviewer, check the git history to find people interested in this topic: 
https://gitlab.com/gitlab-com/www-gitlab-com/-/commits/master/sites/handbook/source/handbook/communication/top-misused-terms/index.html.md
-->
* [ ] Assign to any GitLab team member for initial review and approval. 
* [ ] Assign to the Diversity and Inclusion Manager (`@cwilliams3`) for final review and merge.
