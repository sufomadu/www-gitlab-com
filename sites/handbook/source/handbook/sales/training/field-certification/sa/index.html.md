---
layout: handbook-page-toc
title: "Field Certification: Solutions Architects"
---

# Field Certification Program for Solutions Architects 
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview 
To support and scale GitLab’s continued growth and success, the Field Enablement Team is developing a  certification program for Solutions Architects that includes functional, soft skills, and technical training for field team members.  

Certification badges will align to the customer journey and critical “Moments That Matter” (MTMs) as well as the [field functional competencies](/handbook/sales/training/field-functional-competencies/) that address the critical knowledge, skills, role-based behaviors, processes, content, and tools to successfully execute each MTM.

# Solutions Architects Curriculum 
The below slide shows the holsitic learner journey for SALs, and provides context for the information included throughout the learning program. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/1mhzoJMJSyx4wz47g-0P2dUQJc2hTHflZUhE57mcN83g/edit#slide=id.g94bb3b04a3_0_458" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

# Delivering Value-Driven Demos  
This is the first learning path in development for SAs. The course will consist of 5 courses, the learning objectives for each course are below.

**Course 1: Gather the Technical Requirements through Discovery** 
* Review Customer Requirements document prepared by SDR and SAL
* Perform high-level technical discovery call with customer 

**Course 2: Prepare for the Customer Demo** 
* Analyze the updated requirements from the customer collected during discovery 
* Prepare a pre-demo questionnaire
* Build the customer demo 
* Conduct a demo dry run 

**Course 3: Conduct the Customer Demo** 
* Demonstrate GitLab features, value, propositions, and workflows 
* Present compelling information in a  relevant, tailored, and logical way 

**Course 4: Update Documentation after the Demo** 
* Review and update customer requirements 

*Commercial SAs will have a light-weight version of these modules*


# Recognition
Upon completing each course, the associate will receieve a badge. 

# Feedback 
We want to hear from you! You can follow along with course development by checking out the issues on the [Field Cert Team Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1637426?&label_name[]=field%20certification). 



