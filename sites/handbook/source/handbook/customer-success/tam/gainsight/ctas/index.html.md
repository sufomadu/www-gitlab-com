---
layout: handbook-page-toc
title: "Calls to Action (CTAs)"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

A [CTA](https://support.gainsight.com/Gainsight_NXT/04Cockpit_and_Playbooks/01About/CTAs_Tasks_and_Playbooks_Overview) is a call to action that is tied to a customer and appears in a TAM's cockpit, as well as the account cockpit. CTAs can be created manually at any time, but some will automatically be created for you based on certain events or data.

To create a new CTA, go to your cockpit and click "+ CTA", then fill out the appropriate information. If a CTA will consist of multiple tasks over a period of time, click CTA after saving it, then click the three dots on the top right of the new right sidebar, and click "Add Task". You can add as many as needed and track your progress in the milestone section.

Watch a quick [video on Gainsight CTAs](https://www.youtube.com/watch?v=qkjmTh3Qad4&feature=youtu.be) to learn how to use them, best practices, and tricks.

### Other CTA features

* Tasks from [Timeline](/handbook/customer-success/tam/gainsight/timeline/) will automatically populate under the Cockpit
* Viewing CTAs:
   * Navigate to the C360 and click "Cockpit" to see CTA for just that customer (note: C360 Cockpits do not include success plan objectives)
   * Use the far left nav panel (under "Home") and click "Cockpit" to see all CTAs for all customers (yours or overall)
* Use the dropdown at the top right to select between "My CTAs", "Closed CTAs" (assigned to anyone), and "All CTAs" (assigned to anyone)
* Click the first "Due Date (Due)" button at the top to change the groupings of the CTAs as desired (you can group by Type, Company, Renewal Date, etc.)
* Click the second "Due Date (Due)" button at the top to change the sort order of the CTAs within each group
* Use the filter button at the top right to create and save customized CTA reports, which you can later access in the dropdown

### Types of CTAs

There are 5 "types" of CTAs, which will have different corresponding playbooks, so if you're looking for a particular playbook be sure to select the appropriate type. See below for each type and the corresponding playbooks for that type.

* Risk
  * [Account Triage](/handbook/customer-success/tam/health-score-triage/#triage-cta)
  * Low License Utilization
  * Product Risk
* Expansion
* Lifecycle
  * Commercial Onboarding Email Series
  * Create Success Plan
  * Customer Offboarding
  * [Executive Business Review (EBR)](/handbook/customer-success/tam/ebr/#how-to-prepare-an-ebr)
  * [New Customer Onboarding](/handbook/customer-success/tam/onboarding/)
  * Usage Data Enablement
* Activity
  * [Handoff](/handbook/customer-success/tam/account-handoff/#account-handoff-cta)
* Renewal
  * [Upcoming Renewa](/handbook/customer-success/tam/renewals/)

If the CTA you're opening does not fall into one of the playbook categories, choose the type that makes the most sense. If it's a common CTA, suggest creating a playbook for the it by [opening an issue](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/boards/1731118?&label_name[]=gainsight).

#### CTA Status

CTA statuses are defined below:
* New - brand new CTA, has yet to be started
* Work in progress - CTA currently being worked on by a TAM 
* Closed Success - this CTA was completed with a positive (or intended) outcome
* Closed Lost: Not a Priority - customer doesn't see this as a top objective to pursue
* Closed Lost: Competitor Tech Loss - we lost to a competitor
* Closed Lost: Product Roadblock - customer does not believe they can adopt because GitLab does not ______
* Closed Lost: Not our Dept - requires a different department, typical in siloed organizations
* Closed Lost: Already Solved - this CTA was already completed (e.g., duplicate CTA)





